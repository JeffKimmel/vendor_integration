﻿namespace VendorIntegration
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tbCtrlMain = new VendorIntegration.TabControlSansTab();
            this.tbMain = new VendorIntegration.GradientTabPage();
            this.gradientPanel1 = new VendorIntegration.GradientPanel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnSendData = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHBRegion = new System.Windows.Forms.TextBox();
            this.btnRemoveConnection = new System.Windows.Forms.Button();
            this.btnAddConnection = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbConnection = new System.Windows.Forms.ComboBox();
            this.grpConn0 = new System.Windows.Forms.GroupBox();
            this.btnSaveSettings0 = new System.Windows.Forms.Button();
            this.grpDBConnection = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.btnVerifySettings0 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbAuthType = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbConfig = new VendorIntegration.GradientTabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtConfigUserName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRegisterUser = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtConfigCompanyName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRegisterCompany = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlHeader = new VendorIntegration.GradientPanel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblWelcomeLine = new System.Windows.Forms.Label();
            this.pnlLeft = new VendorIntegration.GradientPanel();
            this.picRFIDLogo = new System.Windows.Forms.PictureBox();
            this.lnkConfig = new VendorIntegration.GradientLinkLabel();
            this.lnkMain = new VendorIntegration.GradientLinkLabel();
            this.tbCtrlMain.SuspendLayout();
            this.tbMain.SuspendLayout();
            this.gradientPanel1.SuspendLayout();
            this.grpConn0.SuspendLayout();
            this.grpDBConnection.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tbConfig.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRFIDLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tbCtrlMain
            // 
            this.tbCtrlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCtrlMain.Controls.Add(this.tbMain);
            this.tbCtrlMain.Controls.Add(this.tbConfig);
            this.tbCtrlMain.Location = new System.Drawing.Point(200, 40);
            this.tbCtrlMain.Name = "tbCtrlMain";
            this.tbCtrlMain.SelectedIndex = 0;
            this.tbCtrlMain.Size = new System.Drawing.Size(756, 513);
            this.tbCtrlMain.TabIndex = 3;
            // 
            // tbMain
            // 
            this.tbMain.Controls.Add(this.gradientPanel1);
            this.tbMain.Controls.Add(this.panel7);
            this.tbMain.endColor = System.Drawing.Color.Gainsboro;
            this.tbMain.gradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.tbMain.Location = new System.Drawing.Point(4, 22);
            this.tbMain.Name = "tbMain";
            this.tbMain.Padding = new System.Windows.Forms.Padding(3);
            this.tbMain.Size = new System.Drawing.Size(748, 487);
            this.tbMain.startColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.tbMain.TabIndex = 0;
            this.tbMain.Text = "Main";
            this.tbMain.UseVisualStyleBackColor = true;
            // 
            // gradientPanel1
            // 
            this.gradientPanel1.Controls.Add(this.lblStatus);
            this.gradientPanel1.Controls.Add(this.btnTest);
            this.gradientPanel1.Controls.Add(this.btnSendData);
            this.gradientPanel1.Controls.Add(this.label4);
            this.gradientPanel1.Controls.Add(this.txtHBRegion);
            this.gradientPanel1.Controls.Add(this.btnRemoveConnection);
            this.gradientPanel1.Controls.Add(this.btnAddConnection);
            this.gradientPanel1.Controls.Add(this.label2);
            this.gradientPanel1.Controls.Add(this.cmbConnection);
            this.gradientPanel1.Controls.Add(this.grpConn0);
            this.gradientPanel1.endColor = System.Drawing.Color.Gainsboro;
            this.gradientPanel1.gradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gradientPanel1.Location = new System.Drawing.Point(5, 85);
            this.gradientPanel1.Name = "gradientPanel1";
            this.gradientPanel1.Size = new System.Drawing.Size(737, 393);
            this.gradientPanel1.startColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientPanel1.TabIndex = 52;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.DimGray;
            this.lblStatus.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblStatus.ForeColor = System.Drawing.Color.White;
            this.lblStatus.Location = new System.Drawing.Point(92, 11);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(542, 47);
            this.lblStatus.TabIndex = 151;
            this.lblStatus.Text = "Status For Integration";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus.Visible = false;
            // 
            // btnTest
            // 
            this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnTest.Enabled = false;
            this.btnTest.FlatAppearance.BorderSize = 0;
            this.btnTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTest.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTest.ForeColor = System.Drawing.Color.White;
            this.btnTest.Location = new System.Drawing.Point(500, 332);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(218, 40);
            this.btnTest.TabIndex = 150;
            this.btnTest.Text = "Test Button";
            this.btnTest.UseVisualStyleBackColor = false;
            this.btnTest.Visible = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnSendData
            // 
            this.btnSendData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSendData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnSendData.FlatAppearance.BorderSize = 0;
            this.btnSendData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendData.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendData.ForeColor = System.Drawing.Color.White;
            this.btnSendData.Location = new System.Drawing.Point(245, 332);
            this.btnSendData.Name = "btnSendData";
            this.btnSendData.Size = new System.Drawing.Size(218, 40);
            this.btnSendData.TabIndex = 140;
            this.btnSendData.Text = "Send To Server";
            this.btnSendData.UseVisualStyleBackColor = false;
            this.btnSendData.Click += new System.EventHandler(this.btnSendData_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 57);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 90;
            this.label4.Text = "Region:";
            // 
            // txtHBRegion
            // 
            this.txtHBRegion.Location = new System.Drawing.Point(80, 55);
            this.txtHBRegion.Margin = new System.Windows.Forms.Padding(2);
            this.txtHBRegion.Name = "txtHBRegion";
            this.txtHBRegion.Size = new System.Drawing.Size(170, 20);
            this.txtHBRegion.TabIndex = 20;
            // 
            // btnRemoveConnection
            // 
            this.btnRemoveConnection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnRemoveConnection.FlatAppearance.BorderSize = 0;
            this.btnRemoveConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveConnection.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveConnection.ForeColor = System.Drawing.Color.White;
            this.btnRemoveConnection.Location = new System.Drawing.Point(509, 20);
            this.btnRemoveConnection.Name = "btnRemoveConnection";
            this.btnRemoveConnection.Size = new System.Drawing.Size(218, 64);
            this.btnRemoveConnection.TabIndex = 40;
            this.btnRemoveConnection.Text = "Remove Current Connection";
            this.btnRemoveConnection.UseVisualStyleBackColor = false;
            this.btnRemoveConnection.Click += new System.EventHandler(this.btnRemoveConnection_Click);
            // 
            // btnAddConnection
            // 
            this.btnAddConnection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnAddConnection.FlatAppearance.BorderSize = 0;
            this.btnAddConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddConnection.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddConnection.ForeColor = System.Drawing.Color.White;
            this.btnAddConnection.Location = new System.Drawing.Point(277, 20);
            this.btnAddConnection.Name = "btnAddConnection";
            this.btnAddConnection.Size = new System.Drawing.Size(218, 64);
            this.btnAddConnection.TabIndex = 30;
            this.btnAddConnection.Text = "Add New Connection";
            this.btnAddConnection.UseVisualStyleBackColor = false;
            this.btnAddConnection.Click += new System.EventHandler(this.btnAddConnection_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Connection:";
            // 
            // cmbConnection
            // 
            this.cmbConnection.FormattingEnabled = true;
            this.cmbConnection.ItemHeight = 13;
            this.cmbConnection.Location = new System.Drawing.Point(80, 25);
            this.cmbConnection.Name = "cmbConnection";
            this.cmbConnection.Size = new System.Drawing.Size(170, 21);
            this.cmbConnection.TabIndex = 10;
            this.cmbConnection.SelectedIndexChanged += new System.EventHandler(this.cmbConnection_SelectedIndexChanged);
            // 
            // grpConn0
            // 
            this.grpConn0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpConn0.Controls.Add(this.btnSaveSettings0);
            this.grpConn0.Controls.Add(this.grpDBConnection);
            this.grpConn0.Controls.Add(this.btnVerifySettings0);
            this.grpConn0.Controls.Add(this.groupBox2);
            this.grpConn0.Location = new System.Drawing.Point(4, 106);
            this.grpConn0.Margin = new System.Windows.Forms.Padding(2);
            this.grpConn0.Name = "grpConn0";
            this.grpConn0.Padding = new System.Windows.Forms.Padding(2);
            this.grpConn0.Size = new System.Drawing.Size(726, 188);
            this.grpConn0.TabIndex = 51;
            this.grpConn0.TabStop = false;
            this.grpConn0.Text = "Database Connection Settings";
            // 
            // btnSaveSettings0
            // 
            this.btnSaveSettings0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveSettings0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnSaveSettings0.FlatAppearance.BorderSize = 0;
            this.btnSaveSettings0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveSettings0.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveSettings0.ForeColor = System.Drawing.Color.White;
            this.btnSaveSettings0.Location = new System.Drawing.Point(438, 138);
            this.btnSaveSettings0.Name = "btnSaveSettings0";
            this.btnSaveSettings0.Size = new System.Drawing.Size(218, 40);
            this.btnSaveSettings0.TabIndex = 130;
            this.btnSaveSettings0.Text = "Save Settings";
            this.btnSaveSettings0.UseVisualStyleBackColor = false;
            this.btnSaveSettings0.Click += new System.EventHandler(this.btnSaveSettings0_Click);
            // 
            // grpDBConnection
            // 
            this.grpDBConnection.Controls.Add(this.label15);
            this.grpDBConnection.Controls.Add(this.txtServer);
            this.grpDBConnection.Controls.Add(this.label16);
            this.grpDBConnection.Controls.Add(this.txtDatabase);
            this.grpDBConnection.Location = new System.Drawing.Point(19, 20);
            this.grpDBConnection.Margin = new System.Windows.Forms.Padding(2);
            this.grpDBConnection.Name = "grpDBConnection";
            this.grpDBConnection.Padding = new System.Windows.Forms.Padding(2);
            this.grpDBConnection.Size = new System.Drawing.Size(347, 108);
            this.grpDBConnection.TabIndex = 45;
            this.grpDBConnection.TabStop = false;
            this.grpDBConnection.Text = "Security Settings";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 22);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "SQL Server Name:";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(142, 20);
            this.txtServer.Margin = new System.Windows.Forms.Padding(2);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(193, 20);
            this.txtServer.TabIndex = 50;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 52);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Database Name:";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(144, 50);
            this.txtDatabase.Margin = new System.Windows.Forms.Padding(2);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(193, 20);
            this.txtDatabase.TabIndex = 60;
            // 
            // btnVerifySettings0
            // 
            this.btnVerifySettings0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVerifySettings0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnVerifySettings0.FlatAppearance.BorderSize = 0;
            this.btnVerifySettings0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerifySettings0.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerifySettings0.ForeColor = System.Drawing.Color.White;
            this.btnVerifySettings0.Location = new System.Drawing.Point(68, 138);
            this.btnVerifySettings0.Name = "btnVerifySettings0";
            this.btnVerifySettings0.Size = new System.Drawing.Size(218, 40);
            this.btnVerifySettings0.TabIndex = 120;
            this.btnVerifySettings0.Text = "Verify Connection";
            this.btnVerifySettings0.UseVisualStyleBackColor = false;
            this.btnVerifySettings0.Click += new System.EventHandler(this.btnVerifySettings0_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.cmbAuthType);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtUserID);
            this.groupBox2.Controls.Add(this.txtPassword);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Location = new System.Drawing.Point(398, 20);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(294, 108);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Security Settings";
            // 
            // cmbAuthType
            // 
            this.cmbAuthType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAuthType.FormattingEnabled = true;
            this.cmbAuthType.Items.AddRange(new object[] {
            "Windows Authentication",
            "SQL Server Authentication"});
            this.cmbAuthType.Location = new System.Drawing.Point(98, 20);
            this.cmbAuthType.Margin = new System.Windows.Forms.Padding(2);
            this.cmbAuthType.Name = "cmbAuthType";
            this.cmbAuthType.Size = new System.Drawing.Size(158, 21);
            this.cmbAuthType.TabIndex = 90;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(26, 50);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "User ID:";
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(98, 50);
            this.txtUserID.Margin = new System.Windows.Forms.Padding(2);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(158, 20);
            this.txtUserID.TabIndex = 100;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(98, 80);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(158, 20);
            this.txtPassword.TabIndex = 110;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(26, 80);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "Password:";
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel7.Controls.Add(this.pictureBox3);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.groupBox5);
            this.panel7.Location = new System.Drawing.Point(5, 5);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(737, 80);
            this.panel7.TabIndex = 50;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(5, 8);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(66, 63);
            this.pictureBox3.TabIndex = 31;
            this.pictureBox3.TabStop = false;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(81, 1);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(649, 80);
            this.label19.TabIndex = 28;
            this.label19.Text = resources.GetString("label19.Text");
            // 
            // groupBox5
            // 
            this.groupBox5.Location = new System.Drawing.Point(76, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(2, 70);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            // 
            // tbConfig
            // 
            this.tbConfig.Controls.Add(this.groupBox4);
            this.tbConfig.Controls.Add(this.groupBox3);
            this.tbConfig.Controls.Add(this.panel1);
            this.tbConfig.endColor = System.Drawing.Color.Gainsboro;
            this.tbConfig.gradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.tbConfig.Location = new System.Drawing.Point(4, 22);
            this.tbConfig.Name = "tbConfig";
            this.tbConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tbConfig.Size = new System.Drawing.Size(748, 487);
            this.tbConfig.startColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.tbConfig.TabIndex = 1;
            this.tbConfig.Text = "Config";
            this.tbConfig.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtConfigUserName);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.btnRegisterUser);
            this.groupBox4.Location = new System.Drawing.Point(77, 290);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(570, 162);
            this.groupBox4.TabIndex = 90;
            this.groupBox4.TabStop = false;
            // 
            // txtConfigUserName
            // 
            this.txtConfigUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfigUserName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtConfigUserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConfigUserName.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfigUserName.Location = new System.Drawing.Point(28, 55);
            this.txtConfigUserName.Multiline = true;
            this.txtConfigUserName.Name = "txtConfigUserName";
            this.txtConfigUserName.Size = new System.Drawing.Size(506, 32);
            this.txtConfigUserName.TabIndex = 92;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(189, 25);
            this.label5.TabIndex = 93;
            this.label5.Text = "Registered User:";
            // 
            // btnRegisterUser
            // 
            this.btnRegisterUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnRegisterUser.FlatAppearance.BorderSize = 0;
            this.btnRegisterUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegisterUser.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegisterUser.ForeColor = System.Drawing.Color.White;
            this.btnRegisterUser.Location = new System.Drawing.Point(316, 100);
            this.btnRegisterUser.Name = "btnRegisterUser";
            this.btnRegisterUser.Size = new System.Drawing.Size(218, 40);
            this.btnRegisterUser.TabIndex = 88;
            this.btnRegisterUser.Text = "Register User";
            this.btnRegisterUser.UseVisualStyleBackColor = false;
            this.btnRegisterUser.Click += new System.EventHandler(this.btnRegisterUser_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtConfigCompanyName);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.btnRegisterCompany);
            this.groupBox3.Location = new System.Drawing.Point(77, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(570, 162);
            this.groupBox3.TabIndex = 89;
            this.groupBox3.TabStop = false;
            // 
            // txtConfigCompanyName
            // 
            this.txtConfigCompanyName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfigCompanyName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtConfigCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConfigCompanyName.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfigCompanyName.Location = new System.Drawing.Point(28, 55);
            this.txtConfigCompanyName.Multiline = true;
            this.txtConfigCompanyName.Name = "txtConfigCompanyName";
            this.txtConfigCompanyName.Size = new System.Drawing.Size(506, 32);
            this.txtConfigCompanyName.TabIndex = 92;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(25, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(238, 25);
            this.label7.TabIndex = 93;
            this.label7.Text = "Registered Company:";
            // 
            // btnRegisterCompany
            // 
            this.btnRegisterCompany.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(163)))), ((int)(((byte)(198)))));
            this.btnRegisterCompany.FlatAppearance.BorderSize = 0;
            this.btnRegisterCompany.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegisterCompany.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegisterCompany.ForeColor = System.Drawing.Color.White;
            this.btnRegisterCompany.Location = new System.Drawing.Point(316, 100);
            this.btnRegisterCompany.Name = "btnRegisterCompany";
            this.btnRegisterCompany.Size = new System.Drawing.Size(218, 40);
            this.btnRegisterCompany.TabIndex = 88;
            this.btnRegisterCompany.Text = "Register Company";
            this.btnRegisterCompany.UseVisualStyleBackColor = false;
            this.btnRegisterCompany.Click += new System.EventHandler(this.btnRegisterCompany_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(737, 80);
            this.panel1.TabIndex = 51;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 63);
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(81, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(649, 58);
            this.label1.TabIndex = 28;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(76, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(2, 70);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            // 
            // pnlHeader
            // 
            this.pnlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHeader.Controls.Add(this.lblDate);
            this.pnlHeader.Controls.Add(this.lblWelcomeLine);
            this.pnlHeader.endColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.pnlHeader.gradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.pnlHeader.Location = new System.Drawing.Point(200, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(756, 40);
            this.pnlHeader.startColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
            this.pnlHeader.TabIndex = 2;
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(628, 9);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(118, 24);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "12/27/1968";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWelcomeLine
            // 
            this.lblWelcomeLine.AutoSize = true;
            this.lblWelcomeLine.BackColor = System.Drawing.Color.Transparent;
            this.lblWelcomeLine.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblWelcomeLine.ForeColor = System.Drawing.Color.White;
            this.lblWelcomeLine.Location = new System.Drawing.Point(18, 9);
            this.lblWelcomeLine.Name = "lblWelcomeLine";
            this.lblWelcomeLine.Size = new System.Drawing.Size(104, 24);
            this.lblWelcomeLine.TabIndex = 0;
            this.lblWelcomeLine.Text = "Welcome";
            this.lblWelcomeLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlLeft
            // 
            this.pnlLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlLeft.Controls.Add(this.picRFIDLogo);
            this.pnlLeft.Controls.Add(this.lnkConfig);
            this.pnlLeft.Controls.Add(this.lnkMain);
            this.pnlLeft.endColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
            this.pnlLeft.gradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(200, 553);
            this.pnlLeft.startColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
            this.pnlLeft.TabIndex = 0;
            // 
            // picRFIDLogo
            // 
            this.picRFIDLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picRFIDLogo.Image = global::VendorIntegration.Properties.Resources.PlainLogo_Dark;
            this.picRFIDLogo.Location = new System.Drawing.Point(41, 28);
            this.picRFIDLogo.Name = "picRFIDLogo";
            this.picRFIDLogo.Size = new System.Drawing.Size(116, 69);
            this.picRFIDLogo.TabIndex = 15;
            this.picRFIDLogo.TabStop = false;
            // 
            // lnkConfig
            // 
            this.lnkConfig.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkConfig.endColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
            this.lnkConfig.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lnkConfig.gradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.lnkConfig.Image = global::VendorIntegration.Properties.Resources.icons8_Clean_24;
            this.lnkConfig.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lnkConfig.LinkColor = System.Drawing.Color.White;
            this.lnkConfig.Location = new System.Drawing.Point(0, 190);
            this.lnkConfig.Name = "lnkConfig";
            this.lnkConfig.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lnkConfig.Size = new System.Drawing.Size(200, 30);
            this.lnkConfig.startColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
            this.lnkConfig.TabIndex = 1;
            this.lnkConfig.TabStop = true;
            this.lnkConfig.Text = "Configuration";
            this.lnkConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lnkConfig.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkConfig_LinkClicked);
            // 
            // lnkMain
            // 
            this.lnkMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkMain.endColor = System.Drawing.Color.Beige;
            this.lnkMain.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lnkMain.gradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.lnkMain.Image = global::VendorIntegration.Properties.Resources.icons8_Clean_24;
            this.lnkMain.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lnkMain.LinkColor = System.Drawing.Color.White;
            this.lnkMain.Location = new System.Drawing.Point(0, 140);
            this.lnkMain.Name = "lnkMain";
            this.lnkMain.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lnkMain.Size = new System.Drawing.Size(200, 40);
            this.lnkMain.startColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.lnkMain.TabIndex = 0;
            this.lnkMain.TabStop = true;
            this.lnkMain.Text = "Main";
            this.lnkMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lnkMain.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkMain_LinkClicked);
            this.lnkMain.Click += new System.EventHandler(this.lnkMain_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 552);
            this.Controls.Add(this.tbCtrlMain);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnlLeft);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Address Book Integration Service";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tbCtrlMain.ResumeLayout(false);
            this.tbMain.ResumeLayout(false);
            this.gradientPanel1.ResumeLayout(false);
            this.gradientPanel1.PerformLayout();
            this.grpConn0.ResumeLayout(false);
            this.grpDBConnection.ResumeLayout(false);
            this.grpDBConnection.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tbConfig.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picRFIDLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private GradientPanel pnlLeft;
        private GradientLinkLabel lnkMain;
        private GradientPanel pnlHeader;
        private TabControlSansTab tbCtrlMain;
        private GradientTabPage tbMain;
        private GradientTabPage tbConfig;
        private GradientLinkLabel lnkConfig;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox picRFIDLogo;
        private System.Windows.Forms.GroupBox grpConn0;
        private System.Windows.Forms.Button btnSaveSettings0;
        private System.Windows.Forms.GroupBox grpDBConnection;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.Button btnVerifySettings0;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbAuthType;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label18;
        private GradientPanel gradientPanel1;
        private System.Windows.Forms.Button btnAddConnection;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbConnection;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblWelcomeLine;
        private System.Windows.Forms.Button btnRegisterCompany;
        private System.Windows.Forms.Button btnRemoveConnection;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtHBRegion;
        private System.Windows.Forms.Button btnSendData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtConfigUserName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRegisterUser;
        private System.Windows.Forms.TextBox txtConfigCompanyName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label lblStatus;
    }
}

