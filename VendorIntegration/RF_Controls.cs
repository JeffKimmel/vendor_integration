﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace VendorIntegration
{
    class RF_Controls
    {
    }
    class TabControlSansTab : System.Windows.Forms.TabControl
    {
        protected override void WndProc(ref Message m)
        {
            // Hide tabs by trapping the TCM_ADJUSTRECT message
            if (m.Msg == 0x1328 && !DesignMode)
                m.Result = (System.IntPtr)1;
            else
                base.WndProc(ref m);
        }
    }

    public class GradientPanel : Panel
    {
        private Color _startColor = Color.Black;
        private Color _endColor = Color.White;
        private LinearGradientMode _gradientMode = LinearGradientMode.ForwardDiagonal;

        public Color startColor
        {
            get { return _startColor; }
            set { _startColor = value; }
        }

        public Color endColor
        {
            get { return _endColor; }
            set { _endColor = value; }
        }

        public LinearGradientMode gradientMode
        {
            get { return _gradientMode; }
            set { _gradientMode = value; }
        }

        public GradientPanel()
        {
            this.ResizeRedraw = true;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if ((this.ClientRectangle.Width == 0) || (this.ClientRectangle.Height == 0))
            {
                return;
            }

            using (var brush = new LinearGradientBrush(this.ClientRectangle,
                       startColor, endColor, gradientMode))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }
        protected override void OnScroll(ScrollEventArgs se)
        {
            this.Invalidate();
            base.OnScroll(se);
        }
    }

    public class GradientMainForm : MainForm
    {
        private Color _startColor = Color.Black;
        private Color _endColor = Color.White;
        private LinearGradientMode _gradientMode = LinearGradientMode.ForwardDiagonal;

        public Color startColor
        {
            get { return _startColor; }
            set { _startColor = value; }
        }

        public Color endColor
        {
            get { return _endColor; }
            set { _endColor = value; }
        }

        public LinearGradientMode gradientMode
        {
            get { return _gradientMode; }
            set { _gradientMode = value; }
        }

        public GradientMainForm()
        {
            this.ResizeRedraw = true;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if ((this.ClientRectangle.Width == 0) || (this.ClientRectangle.Height == 0))
            {
                return;
            }

            using (var brush = new LinearGradientBrush(this.ClientRectangle,
                       startColor, endColor, gradientMode))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }
        protected override void OnScroll(ScrollEventArgs se)
        {
            this.Invalidate();
            base.OnScroll(se);
        }
    }

    public class GradientTabPage : TabPage
    {
        private Color _startColor = Color.White;
        private Color _endColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
        private LinearGradientMode _gradientMode = LinearGradientMode.ForwardDiagonal;

        public Color startColor
        {
            get { return _startColor; }
            set { _startColor = value; }
        }

        public Color endColor
        {
            get { return _endColor; }
            set { _endColor = value; }
        }

        public LinearGradientMode gradientMode
        {
            get { return _gradientMode; }
            set { _gradientMode = value; }
        }

        public GradientTabPage()
        {
            this.ResizeRedraw = true;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if ((this.ClientRectangle.Width == 0) || (this.ClientRectangle.Height == 0))
            {
                return;

            }
            using (var brush = new LinearGradientBrush(this.ClientRectangle,
                       startColor, endColor, gradientMode))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }
        protected override void OnScroll(ScrollEventArgs se)
        {
            this.Invalidate();
            base.OnScroll(se);
        }
    }

    public class GradientLabel : Label
    {
        private Color _startColor = Color.Black;
        private Color _endColor = Color.White;
        private LinearGradientMode _gradientMode = LinearGradientMode.ForwardDiagonal;

        public Color startColor
        {
            get { return _startColor; }
            set { _startColor = value; }
        }

        public Color endColor
        {
            get { return _endColor; }
            set { _endColor = value; }
        }

        public LinearGradientMode gradientMode
        {
            get { return _gradientMode; }
            set { _gradientMode = value; }
        }

        public GradientLabel()
        {
            this.ResizeRedraw = true;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if ((this.ClientRectangle.Width == 0) || (this.ClientRectangle.Height == 0))
            {
                return;
            }

            using (var brush = new LinearGradientBrush(this.ClientRectangle,
                       startColor, endColor, gradientMode))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }
        //protected override void OnScroll(ScrollEventArgs se)
        //{
        //    this.Invalidate();
        //    base.OnScroll(se);
        //}
    }

    public class GradientLinkLabel : LinkLabel
    {
        private Color _startColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
        private Color _endColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
        private LinearGradientMode _gradientMode = LinearGradientMode.Horizontal;

        public Color startColor
        {
            get { return _startColor; }
            set { _startColor = value; }
        }

        public Color endColor
        {
            get { return _endColor; }
            set { _endColor = value; }
        }

        public LinearGradientMode gradientMode
        {
            get { return _gradientMode; }
            set { _gradientMode = value; }
        }

        public GradientLinkLabel()
        {
            this.ResizeRedraw = true;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if ((this.ClientRectangle.Width == 0) || (this.ClientRectangle.Height == 0))
            {
                return;
            }

            using (var brush = new LinearGradientBrush(this.ClientRectangle,
                       startColor, endColor, gradientMode))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }
    }

    public class GradientButton : Button
    {
        private Color _startColor = Color.Black;
        private Color _endColor = Color.White;
        private LinearGradientMode _gradientMode = LinearGradientMode.ForwardDiagonal;

        public Color startColor
        {
            get { return _startColor; }
            set { _startColor = value; }
        }

        public Color endColor
        {
            get { return _endColor; }
            set { _endColor = value; }
        }

        public LinearGradientMode gradientMode
        {
            get { return _gradientMode; }
            set { _gradientMode = value; }
        }

        public GradientButton()
        {
            this.ResizeRedraw = true;
        }

        /*        protected override void OnPaint(PaintEventArgs e)
                protected override void OnPaintBackground(PaintEventArgs e)
                {
                    base.OnPaint(e);
                    if ((this.ClientRectangle.Width == 0) || (this.ClientRectangle.Height == 0))
                    {
                        return;
                    }

                    using (var brush = new LinearGradientBrush(this.ClientRectangle,
                               startColor, endColor, gradientMode))
                    {
                        e.Graphics.FillRectangle(brush, this.ClientRectangle);
                    }

                }
         */


        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;

            if ((this.ClientRectangle.Width == 0) || (this.ClientRectangle.Height == 0))
            {
                return;
            }

            g.FillRectangle(
            new LinearGradientBrush(PointF.Empty, new PointF(0, this.Height), startColor, endColor),
            new RectangleF(PointF.Empty, this.Size));

            //using (var brush = new LinearGradientBrush(g.ClientRectangle,
            //           startColor, endColor, gradientMode))
            //{
            //    e.Graphics.FillRectangle(brush, this.ClientRectangle);
            //}
        }
    }

}
