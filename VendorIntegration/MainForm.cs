﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using VendorIntegration.Class;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Diagnostics;

namespace VendorIntegration
{
    public partial class MainForm : Form
    {
        registrationObject regObj;
        Color startLnkColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
        Color endLnkColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
        Color startLinkColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
        Color endLinkColorSelected = Color.Beige;
        HttpClient RFIDWebService;
        List<svcBU> allRegionList;
        List<svcConn> allConnections;
        
        public MainForm()
        {
            InitializeComponent();
        }

        public void showMain()
        {
            tbCtrlMain.SelectedTab = tbMain;
        }

        public void showConfig()
        {
            txtConfigCompanyName.Text = regObj.companyName;
            txtConfigUserName.Text = regObj.userFirst + " " + regObj.userLast;
            tbCtrlMain.SelectedTab = tbConfig;

        }

        public void linkClicked(GradientLinkLabel obj)
        {
            lnkConfig.startColor = startLnkColorNormal;
            lnkConfig.endColor = endLnkColorNormal;
            lnkConfig.Invalidate();

            lnkMain.startColor = startLnkColorNormal;
            lnkMain.endColor = endLnkColorNormal;
            lnkMain.Invalidate();


            if (obj == lnkConfig)
            {
                showConfig();   
            }
            else if (obj == lnkMain)
            {
                showMain();
            }

            obj.startColor = startLinkColorSelected;
            obj.endColor = endLinkColorSelected;
            obj.Invalidate();
        }

        private void lnkMain_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkClicked(lnkMain);
        }

        private void lnkConfig_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkClicked(lnkConfig);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Now.ToShortDateString();
            regObj = clsRegistration.loadConfigFile();
            if (regObj.success)
            {
                lblWelcomeLine.Text = "Welcome " + regObj.userFirst + " " + regObj.userLast;
            }
            allConnections = clsConnections.loadConnections().connList;
            
            RFIDWebService = new HttpClient();
            RFIDWebService.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            RFIDWebService.BaseAddress = new Uri(regObj.server);
            
            string[] args = Environment.GetCommandLineArgs();
            
            //Had this in here for testing!!!
            //foreach (string me in args)
            //{
            //    //MessageBox.Show(me);
            //}
            if ((args.Length > 1) && (args[1].CompareTo("1") == 0))
            {
                sendDataToServer();
                Application.Exit();
            }

            if (allConnections.Count > 0)
            {
                refillConectionCombo(allConnections[0].name);
            }
            else
            {
                refillConectionCombo("");
            }
            if (!regObj.success)
            {
                MessageBox.Show(regObj.errMsg + "  Please contact support as this is unusual.", clsConstants.ERRHDR);
            }
        }

        private void btnRegisterCompany_Click(object sender, EventArgs e)
        {
            Register reg = new Register();
            reg.m_RegisterCompany = true;
            reg.RFIDWebService = RFIDWebService;
            reg.ShowDialog();            
            if (reg.DialogResult == DialogResult.OK)
            {
                regObj = clsRegistration.loadConfigFile();
            }
            reg.Dispose();
        }

        private void lnkMain_Click(object sender, EventArgs e)
        {
            linkClicked(lnkMain);
        }

        private void btnAddConnection_Click(object sender, EventArgs e)
        {
            List<svcBU> passedInList = new List<svcBU>();

            if (allRegionList == null)
            {
                svcBUReturn buRet = clsRegions.regionList(RFIDWebService, regObj.companyID.ToString(), regObj.token);
                if (!buRet.success)
                {
                    MessageBox.Show("Could not upload regions: " + buRet.errors[0], clsConstants.ERRHDR);
                    return;
                }
                allRegionList = buRet.buList;
            }

            //Cycle through the the connections to fill in what we should show in the popup.
            //do this here because if we've seelcted them all we need to not pop up anythign
            foreach (svcBU theBU in allRegionList)
            {
                bool addRow = true;
                foreach (svcConn oneConn in allConnections)
                {
                    if (oneConn.buID.CompareTo(theBU.id.ToString()) == 0)
                    {
                        addRow = false;
                        break;
                    }
                }
                if (addRow)
                {
                    passedInList.Add(theBU);
                }
            }
            if (passedInList.Count == 0)
            {
                MessageBox.Show("All connections have been setup, cannot add another connection.", clsConstants.ERRHDR);
                return;
            }


            AddConnection add = new AddConnection();
            add.regionList = passedInList;
            
            add.ShowDialog();
            if (add.DialogResult == DialogResult.OK)
            {
                svcBU selBU = add.selectedBU;
                if (selBU != null)
                {
                    svcConn newConn = clsConnections.initConn();
                    newConn.buID = selBU.id.ToString();
                    newConn.name = selBU.buName;
                    allConnections.Add(newConn);
                    refillConectionCombo(selBU.buName);
                }
            }
            add.Dispose();

        }

        private void refillConectionCombo(string defConnectionName)
        {
            cmbConnection.Items.Clear();
            foreach (svcConn oneConn in allConnections)
            {
                cmbConnection.Items.Add(oneConn.name);
            }
            cmbConnection.Text = defConnectionName;
        }

        private void btnSaveSettings0_Click(object sender, EventArgs e)
        {
            if (allRegionList == null)
            {
                svcBUReturn buRet = clsRegions.regionList(RFIDWebService, regObj.companyID.ToString(), regObj.token);
                if (!buRet.success)
                {
                    MessageBox.Show("Could not upload regions: " + buRet.errors[0], clsConstants.ERRHDR);
                    return;
                }
                allRegionList = buRet.buList;
            }

            string sRegion = cmbConnection.Text;
            //Doesn't really matter if we get false or true, we just need it to return the list
            allConnections = clsConnections.removeConnection(allConnections, sRegion).connList;
            svcBUReturn buRetObj = clsRegions.regionFromName(allRegionList, sRegion);
            if (!buRetObj.success)
            {
                MessageBox.Show("Could not find the selected business unit.  Please reload the business units and try again.", clsConstants.ERRHDR);
                return;
            }
            svcConn newConn = clsConnections.initConn();
            newConn.authType = cmbAuthType.Text;
            newConn.buID = buRetObj.oneBU.id.ToString();
            newConn.buToken = buRetObj.oneBU.buToken;
            newConn.compToken = regObj.token;
            newConn.HBRegion = txtHBRegion.Text;
            newConn.database = txtDatabase.Text;
            newConn.name = sRegion;
            newConn.server = txtServer.Text;
            newConn.userName = txtUserID.Text;
            newConn.userPW = txtPassword.Text;
            allConnections.Add(newConn);

            svcConnResponse connResp = clsConnections.saveConnections(allConnections);
            refillConectionCombo(sRegion);
            cmbConnection.Text = sRegion;
            if (!connResp.success)
            {
                MessageBox.Show("Could not save connection file, the reported error was: " + connResp.message + ".  Please try again.  If the error persists please call support.", clsConstants.ERRHDR);
            }
 
        }

        private void btnVerifySettings0_Click(object sender, EventArgs e)
        {
            svcReturn me = clsConnections.testConnection(txtServer.Text, txtDatabase.Text, cmbAuthType.Text, txtUserID.Text, txtPassword.Text);
            MessageBox.Show(me.errors[0], clsConstants.ERRHDR);
        }

        private void cmbConnection_SelectedIndexChanged(object sender, EventArgs e)
        {
            svcConnResponse connResp = clsConnections.findConnection(allConnections, cmbConnection.Text);
            txtDatabase.Text = "";
            cmbAuthType.Text = "";
            txtPassword.Text = "";
            txtServer.Text = "";
            txtUserID.Text = "";
            txtHBRegion.Text = "";

            if (!connResp.success)
            {
                MessageBox.Show("Could not find the selected connection.  Please try again, if the problem persists please call support as this is unusual.", clsConstants.ERRHDR);
                return;
            }
            try { txtDatabase.Text = connResp.oneConnection.database; } catch (Exception) { }
            try { cmbAuthType.Text = connResp.oneConnection.authType; } catch (Exception) { }
            try { txtPassword.Text = connResp.oneConnection.userPW; } catch (Exception) { }
            try { txtServer.Text = connResp.oneConnection.server; } catch (Exception) { }
            try { txtUserID.Text = connResp.oneConnection.userName; } catch (Exception) { }
            try { txtHBRegion.Text = connResp.oneConnection.HBRegion; } catch (Exception) { }
        }

        private void btnRemoveConnection_Click(object sender, EventArgs e)
        {
            if (cmbConnection.Text.CompareTo("") == 0)
            {
                MessageBox.Show("Please select a region to remove and try again.", clsConstants.ERRHDR);
                return;
            }
            DialogResult res = MessageBox.Show("Remove the selected region?", clsConstants.ERRHDR, MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
                svcConnResponse connResp = clsConnections.removeConnection(allConnections, cmbConnection.Text);
                if (!connResp.success)
                {
                    MessageBox.Show("Could not remove the selected connection, the reported error was : " + connResp.message, clsConstants.ERRHDR);
                }
                else 
                {
                    allConnections = connResp.connList;
                    clsConnections.saveConnections(allConnections);
                    refillConectionCombo("");
                    txtDatabase.Text = "";
                    txtPassword.Text = "";
                    txtServer.Text = "";
                    txtUserID.Text = "";
                    cmbAuthType.Text = "";
                    txtHBRegion.Text = "";
                }
            }
        }

        private void btnSendData_Click(object sender, EventArgs e)
        {
            sendDataToServer();
        }

        private void btnRegisterUser_Click(object sender, EventArgs e)
        {
            Register reg = new Register();
            reg.RFIDWebService = RFIDWebService;
            reg.m_RegisterCompany = false;
            reg.regObj = regObj;
            reg.ShowDialog();

            if (reg.DialogResult == DialogResult.OK)
            {
                regObj = clsRegistration.loadConfigFile();
            }
            reg.Dispose();

        }

        private void sendDataToServer()
        {
            lblStatus.Text = "Updating Products...";
            lblStatus.Visible = true;
            lblStatus.Invalidate();
            clsMessageClass.initiateVendorUpload(RFIDWebService, regObj.companyID.ToString(), regObj.token, regObj.userID.ToString(), regObj.userToken);

            List<messageObject> msgList = new List<messageObject>();
            msgList.Add(clsMessageClass.createMsgObj(regObj, "Update Vendors", "Upload Started", true));
            svcReturn prodReturn = svcProductClass.saveAllProducts(RFIDWebService, allConnections, regObj);
            lblStatus.Text = "Updating Vendors...";
            lblStatus.Invalidate();
//            svcReturn locReturn = clsShared.initValidReturn();
            svcReturn locReturn = svcLocationClass.saveAllLocations(RFIDWebService, allConnections, regObj, lblStatus);
            lblStatus.Text = "Updating Complete...";
            lblStatus.Invalidate();
            
            if (!prodReturn.success)
            {
                msgList[0].Successful = false;
                foreach (string oneErr in prodReturn.errors)
                {
                    msgList.Add(clsMessageClass.createMsgObj(regObj, "Update Vendors", oneErr, false));
                }
            }
            if (!locReturn.success) 
            {
                msgList[0].Successful = false;
                foreach (string oneErr in locReturn.errors)
                {
                    msgList.Add(clsMessageClass.createMsgObj(regObj, "Update Vendors", oneErr, false));
                }
            }
            messageObject last = clsMessageClass.createMsgObj(regObj, "Update Vendors", "Upload Complete", msgList[0].Successful);
            msgList.Add(last);
            svcReturn retObj = clsMessageClass.saveMessageList(RFIDWebService, msgList);
            clsMessageClass.completeVendorUpload(RFIDWebService, regObj.companyID.ToString(), regObj.token, regObj.userID.ToString(), regObj.userToken, msgList[0].Successful);

            Thread.Sleep(1000);
            lblStatus.Visible = false;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            List<messageObject> listObj = new List<messageObject>();
            messageObject req1 = clsMessageClass.createMsgObj(regObj, "Update Vendors", "Start Upload", true);
            messageObject req2 = clsMessageClass.createMsgObj(regObj, "Update Vendors", "Some Middle Bit", true);
            messageObject req3 = clsMessageClass.createMsgObj(regObj, "Update Vendors", "Upload Complete", true); 
                

            listObj.Add(req1);
            listObj.Add(req2);
            listObj.Add(req3);
            svcReturn retObj = clsMessageClass.saveMessageList(RFIDWebService, listObj);
 //           svcReturn retObj = clsMessageClass.saveMessage(RFIDWebService, req1);
            MessageBox.Show(retObj.success.ToString());

        }
    }
}
