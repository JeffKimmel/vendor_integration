﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using VendorIntegration;

namespace VendorIntegration.Class
{
    class clsRegions
    {
        public static svcBUReturn regionList(HttpClient RFIDWebService, string compID, string compToken)
        {
            svcBUReturn resp = new svcBUReturn();
            resp.errors = new List<string>();
            //     [ActionName("RegisterCompany")]
            //public RegisterSoftwareResponse RegisterCompany(String compName, string compName)
            //RegionList?compID=b05aaf8a-3725-4609-a3fe-059f6af9c3fe&compToken=10a4
            
            String url = "api/Cred/RegionList?compID=" + compID + "&compToken=" + compToken;
            HttpResponseMessage response = RFIDWebService.GetAsync(url).Result;
            if (!response.IsSuccessStatusCode)
            {
                resp.success = false;
                resp.errors.Add(response.ReasonPhrase);
            }
            else
            {
                resp.success = true;
                resp = response.Content.ReadAsAsync<svcBUReturn>().Result;
            }
            return resp;
        }

        public static svcBUReturn regionFromID(List<svcBU> allRegionList, Guid regionGUID)
        {
            svcBUReturn retBU = new svcBUReturn();
            retBU.success = false;

            foreach(svcBU thisBU in allRegionList)
            {
                if (thisBU.id == regionGUID)
                {
                    retBU.success = true;
                    retBU.oneBU = thisBU;
                    break;
                }
            }
            return retBU;
        }
  
        public static svcBUReturn regionFromName(List<svcBU> allRegionList, String regionName)
        {
            svcBUReturn retBU = new svcBUReturn();
            retBU.success = false;

            foreach (svcBU thisBU in allRegionList)
            {
                if (thisBU.buName.CompareTo(regionName) == 0)
                {
                    retBU.success = true;
                    retBU.oneBU = thisBU;
                    break;
                }
            }
            return retBU;
        }
    }
}

public class svcBU
{
    public Guid companyID { get; set; }
    public string companyName { get; set; }
    public Guid id { get; set; }
    public string buName { get; set; }
    public string buToken { get; set; }
}

public class svcBUReturn
{
    public bool success { get; set; }
    public List<string> errors;
    public svcBU oneBU { get; set; }
    public List<svcBU> buList { get; set; }
}