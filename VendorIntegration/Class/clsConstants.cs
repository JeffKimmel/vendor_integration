﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorIntegration.Class
{
    class clsConstants
    {
        public const string FILE_Config = "Config.xml";
        public const string FILE_Conn = "Connections.xml";

        public const string ERRHDR = "Address Book Integration Service";
    }
}
