﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using VendorIntegration;

namespace VendorIntegration.Class
{
    class clsRegistration
    {
        public static RegisterSoftwareResponse regSoftware(HttpClient RFIDWebService, string compID, string compToken)
        {
            RegisterSoftwareResponse resp = new RegisterSoftwareResponse();
            //     [ActionName("RegisterCompany")]
            //public RegisterSoftwareResponse RegisterCompany(String compName, string compName)
 
            String url = "api/Cred/RegisterCompany?compName=" + compID + "&token=" + compToken;
            HttpResponseMessage response = RFIDWebService.GetAsync(url).Result;
            if (!response.IsSuccessStatusCode)
            {
                resp.success = false;
                resp.errMsg = response.ReasonPhrase;
            }
            else
            {
                resp = response.Content.ReadAsAsync<RegisterSoftwareResponse>().Result;
            }
            return resp;
        }

        public static svcReturn saveCompanyRegistration(RegisterSoftwareResponse obj, string token)
        {
            XmlDocument xmlDoc = new XmlDocument(); //* create an xml document object.
            xmlDoc.Load(clsConstants.FILE_Config);

            xmlDoc.SelectSingleNode("Config/CompanyName").InnerText = clsCrypto.EncryptString(obj.companyName);
            xmlDoc.SelectSingleNode("Config/CompanyGUID").InnerText = clsCrypto.EncryptString(obj.companyID.ToString());
            xmlDoc.SelectSingleNode("Config/Token").InnerText = clsCrypto.EncryptString(token);
            
            xmlDoc.SelectSingleNode("Config/UserFirst").InnerText = clsCrypto.EncryptString("");
            xmlDoc.SelectSingleNode("Config/UserLast").InnerText= clsCrypto.EncryptString("");
            xmlDoc.SelectSingleNode("Config/UserToken").InnerText = clsCrypto.EncryptString("");
            xmlDoc.SelectSingleNode("Config/UserID").InnerText= clsCrypto.EncryptString("");

            try
            {
                xmlDoc.Save(clsConstants.FILE_Config);                
            }
            catch (Exception ex)
            {
                return clsShared.initInvalidReturn("" + ex.GetBaseException());
            }
            
            return clsShared.initValidReturn();
        }

        public static svcReturn saveUserRegistration(RegisterSoftwareResponse obj)
        {
            XmlDocument xmlDoc = new XmlDocument(); //* create an xml document object.
            xmlDoc.Load(clsConstants.FILE_Config);

            xmlDoc.SelectSingleNode("Config/UserFirst").InnerText = clsCrypto.EncryptString(obj.firstName);
            xmlDoc.SelectSingleNode("Config/UserLast").InnerText = clsCrypto.EncryptString(obj.lastName);
            xmlDoc.SelectSingleNode("Config/UserToken").InnerText = clsCrypto.EncryptString(obj.passCode);
            xmlDoc.SelectSingleNode("Config/UserID").InnerText = clsCrypto.EncryptString(obj.ID.ToString());
            xmlDoc.SelectSingleNode("Config/DivisionGUID").InnerText = clsCrypto.EncryptString(obj.divisionID.ToString());
            xmlDoc.SelectSingleNode("Config/DivisionName").InnerText = clsCrypto.EncryptString(obj.divisionName);

            try
            {
                xmlDoc.Save(clsConstants.FILE_Config);
            }
            catch (Exception ex)
            {
                return clsShared.initInvalidReturn("" + ex.GetBaseException());
            }

            return clsShared.initValidReturn();
        }
      
        public static registrationObject loadConfigFile()
        {
            registrationObject retObj = new registrationObject();
            XmlDocument xmlDoc = new XmlDocument(); //* create an xml document object.
            xmlDoc.Load(clsConstants.FILE_Config);
            try
            {
                retObj.companyID = Guid.Parse(clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/CompanyGUID").InnerText));
                retObj.companyName = clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/CompanyName").InnerText);
                retObj.server = xmlDoc.SelectSingleNode("Config/Server").InnerText;
                retObj.token = clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/Token").InnerText);
                retObj.divisionID = Guid.Parse(clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/DivisionGUID").InnerText));
                retObj.divisionName = clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/DivisionName").InnerText);
                retObj.userFirst = "";
                retObj.userLast = "";
                retObj.userToken = "";
                retObj.userID = "";
                try
                {
                    retObj.userFirst = clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/UserFirst").InnerText);
                }
                catch (Exception) { }
                try
                {
                    retObj.userLast = clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/UserLast").InnerText);
                }
                catch (Exception) { }
                try
                {
                    retObj.userToken = clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/UserToken").InnerText);
                }
                catch (Exception) { }
                try
                {
                    String me = xmlDoc.SelectSingleNode("Config/UserID").InnerText;
                    retObj.userID = clsCrypto.DecryptString(xmlDoc.SelectSingleNode("Config/UserID").InnerText);
                }
                catch (Exception) { }


                retObj.success = true;
            }
            catch (Exception ex)
            {
                retObj.success = false;
                retObj.errMsg = "Could not parse config file: " + ex.Message;
            }
            return retObj;
        }

        public static RegisterSoftwareResponse loginUser(HttpClient RFIDWebService, string compID, string userName, string userPW)
        {
            RegisterSoftwareResponse regResp = new RegisterSoftwareResponse();
            String url = "api/Cred/LoginUser?userLogin=" + userName + "&userPassword=" + userPW + "&compID=" + compID;
            try
            {
                HttpResponseMessage response = RFIDWebService.GetAsync(url).Result;
                if (!response.IsSuccessStatusCode)
                {
                    regResp.success = false;
                    regResp.errMsg = response.ReasonPhrase;
                }
                else
                {
                    regResp = response.Content.ReadAsAsync<RegisterSoftwareResponse>().Result;
                }
            }
            catch (Exception ex)
            {
                regResp.success = false;
                regResp.errMsg = "Unhandled exception, could not verify user: " + ex.GetBaseException();
            }
            return regResp;
        
        }

    }

 }

public class RegisterSoftwareResponse
{
    public string errMsg;
    public bool success;
    public Guid companyID;
    public string companyName;
    public Guid divisionID;
    public string divisionName;
    public Guid userID;
    public string firstName;
    public string lastName;
    public string passCode;
    public string vendCode;
    public string buCredential;
    public Guid ID;
}

public class registrationObject
{
    public string errMsg;
    public bool success;
    public Guid companyID;
    public string companyName;
    public Guid divisionID;
    public string divisionName;
    public string server;
    public string token;
    public string userID;
    public string userFirst;
    public string userLast;
    public string userToken;
}

