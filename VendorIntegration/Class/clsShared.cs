﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendorIntegration.Class
{
    class clsShared
    {
        public static svcReturn initValidReturn()
        {
            svcReturn retObj = new svcReturn();
            retObj.success = true;
            retObj.errors = new List<string>();
            return retObj;
        }

        public static svcReturn initInvalidReturn(string errMsg)
        {
            svcReturn retObj = new svcReturn();
            retObj.success = false;
            retObj.errors = new List<string>();
            retObj.errors.Add(errMsg);
            return retObj;
        }

        public static svcReturn initInvalidReturn(List<string> errors)
        {
            svcReturn retObj = new svcReturn();
            retObj.success = false;
            retObj.errors = errors;
            return retObj;
        }
    }

}


public class svcReturn
{
    public bool success;
    public List<string> errors;
}
