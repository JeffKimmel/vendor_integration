﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Data.SqlClient;

namespace VendorIntegration.Class
{
    class svcProductClass
    {

        public static svcReturn sendProductToServer(HttpClient RFIDWebService, registrationObject regObj, string prodCode, string prodDesc, string buID)
        {
            svcAProductReq req = new svcAProductReq();
            req.prod = new svcAProduct();
            svcReturn result = clsShared.initValidReturn();
            String url = "api/Cred/addUpdateProduct";
            req.userGUID = "62e26029-b107-40a0-8d02-00de71f11d56";
            req.passCode = "10a4";
            req.prod.MAT_CODE = prodCode;
            req.prod.PROD_NAME = prodDesc;
            req.prod.PHASE_CODE = "";
            req.prod.JOB_CODE = "";
            req.prod.CUST_CODE = "";
            req.prod.DivisionID = Guid.Parse(buID);
            try
            {
                HttpResponseMessage webResponse = RFIDWebService.PostAsJsonAsync(url, req).Result;

                if (webResponse.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    result = webResponse.Content.ReadAsAsync<svcReturn>().Result;
                }
                else
                {
                    result.errors.Add("Could not call web service 100.  The reported error is: " + webResponse.ReasonPhrase);
                    result.success = false;
                }
            }
            catch (Exception ex)
            {
                result.errors.Add("Unhandled exception calling web service.  The reported error is: " + ex.GetBaseException());
                result.success = false;
            }
            return result;
        }

        public static svcReturn saveProductsForRegion(HttpClient RFIDWebService, registrationObject regObj, svcConn oneConn)
        {
            svcReturn retObj = clsShared.initValidReturn();
            bool integratedSec = !(oneConn.authType.CompareTo("SQL Server Authentication") == 0);

            String connectionString = ("server=" + oneConn.server);
            connectionString = connectionString + ";database=" + oneConn.database;
            connectionString = connectionString + ";Integrated Security=" +  integratedSec.ToString();

            connectionString = connectionString + "; Uid=" + oneConn.userName + "; Pwd=" + oneConn.userPW;
           
            string sql = "SELECT * FROM ESTPROD";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                string msg = "Could not connect to database.";
                try
                {
                    SqlCommand comm = new SqlCommand(sql, conn);
                    conn.Open();
                    SqlDataReader theReader = comm.ExecuteReader();
                    while (theReader.Read())
                    {
                        string sProd = theReader["Product"].ToString().Trim();
                        string sDesc = theReader["Desc"].ToString().Trim();
                        if (sProd.Trim().CompareTo("*") != 0)
                        {
                            svcReturn tempRet = sendProductToServer(RFIDWebService, regObj, sProd, sDesc, oneConn.buID);
                            if (!tempRet.success)
                            {
                                retObj.success = false;
                                retObj.errors.Add("Could not save " + sProd + ": " + tempRet.errors[0]);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    retObj.success = false;
                    retObj.errors.Add(msg + ".  The reported error was: " + ex.Message);
                }

            }
            return retObj;
        }

        public static svcReturn saveAllProducts(HttpClient RFIDWebService, List<svcConn> allConnections, registrationObject regObj)
        {
            svcReturn retObj = clsShared.initValidReturn();
            foreach (svcConn oneConn in allConnections)
            {
                svcReturn tempObj = saveProductsForRegion(RFIDWebService, regObj, oneConn);
                if (!tempObj.success)
                {
                    retObj.success = false;
                    retObj.errors.AddRange(tempObj.errors);
                }
            }
            return retObj;
        }
        
    }
}


public class svcAProduct
{
    public Guid ID { get; set; }
    public Guid DivisionID { get; set; }
    public string CUST_CODE { get; set; }
    public string JOB_CODE { get; set; }
    public string PHASE_CODE { get; set; }
    public string MAT_CODE { get; set; }
    public string PHASE_NAME { get; set; }
    public string PROD_NAME { get; set; }
    public string JOBCOST { get; set; }
    public Int32 TRUCKS { get; set; }
    public Int32 RLOADS { get; set; }
    public decimal REQ { get; set; }
    public decimal PRICE { get; set; }
    public decimal HAUL_RATE { get; set; }
    public decimal HOUR_HAUL { get; set; }
    public decimal USE_TAX { get; set; }
    public string STATUS { get; set; }
    public Int32 LOADS { get; set; }
    public decimal TONS { get; set; }
    public Int32 DLOADS { get; set; }
    public decimal DTONS { get; set; }
    public string CLOSED { get; set; }
    public DateTime DATE_MOD { get; set; }
    public Nullable<System.Guid> LocationID { get; set; }
    public decimal PRODUCTION { get; set; }
    public string PROD_TYPE { get; set; }

}

public class svcAProductReq
{
    public string userGUID { get; set; }
    public string passCode { get; set; }
    public svcAProduct prod { get; set; }
}