﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Data.Odbc;
using System.Data.SqlClient;
using VendorIntegration;

namespace VendorIntegration.Class
{
    class clsConnections
    {
        public static svcConn initConn()
        {
            svcConn retCon = new svcConn();
            retCon.name = "";
            retCon.HBRegion = "";
            retCon.server  = "";
            retCon.database  = "";
            retCon.authType  = "";
            retCon.userName  = "";
            retCon.userPW  = "";
            retCon.buID  = "";
            retCon.compToken  = "";
            retCon.buToken  = "";
            return retCon; 
        }

        public static svcConnResponse saveConnections(List<svcConn> connList)
        {
            svcConnResponse resp = new svcConnResponse();
            resp.success = true;

            XmlTextWriter writer = new XmlTextWriter(clsConstants.FILE_Conn, System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("AllCons");
            foreach (svcConn oneConn in connList)
            {
                createNode(writer, oneConn.name, oneConn.buID, oneConn.server, oneConn.database, oneConn.authType, oneConn.userName, oneConn.userPW, oneConn.buToken, oneConn.HBRegion);
                
            }
            //createNode(writer, "Northern California", Guid.Empty.ToString(), "PC\\SQLEXPRESS", "Credentials", "SQL Authentication", "sa", "!!gowings!!");
            //createNode(writer, "Southern California", Guid.Empty.ToString(), "PC\\SQLEXPRESS", "Credentials", "SQL Authentication", "sa", "!!gowings!!");
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

            return resp;
        }

        public static svcConnResponse loadConnections()
        {
            svcConnResponse resp = new svcConnResponse();
            resp.connList = new List<svcConn>();
            try
            {
                XmlDocument xmlDoc = new XmlDocument(); //* create an xml document object.
                xmlDoc.Load(clsConstants.FILE_Conn);

                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/AllCons/Con");
                foreach (XmlNode node in nodeList)
                {
                    svcConn conn = new svcConn();
                    conn.authType = clsCrypto.DecryptString(node.SelectSingleNode("AuthType").InnerText);
                    conn.buID = clsCrypto.DecryptString(node.SelectSingleNode("RegionID").InnerText);
                    conn.HBRegion = "";  
                    conn.database = clsCrypto.DecryptString(node.SelectSingleNode("DataBase").InnerText);
                    conn.name = clsCrypto.DecryptString(node.SelectSingleNode("Name").InnerText);
                    conn.server = clsCrypto.DecryptString(node.SelectSingleNode("Server").InnerText);
                    conn.userName = clsCrypto.DecryptString(node.SelectSingleNode("UserName").InnerText);
                    conn.userPW = clsCrypto.DecryptString(node.SelectSingleNode("UserPW").InnerText);
                    conn.buToken = clsCrypto.DecryptString(node.SelectSingleNode("RegionToken").InnerText);
                    try
                    {
                        conn.HBRegion = clsCrypto.DecryptString(node.SelectSingleNode("ServerRegion").InnerText);
                    }
                    catch (Exception)
                    {
                        conn.HBRegion = "";
                    }
                    resp.connList.Add(conn);
                }
                resp.success = true;
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.message = "Could not load connection file: " + ex.GetBaseException() + ".  Please call support as this is unusual.";
            }
            return resp;
        }

        private static void createNode(XmlTextWriter writer, string buName, string buID ,string Server, string thisDB, string authType, string userName, string userPW, string buToken, string connNameOnServer)
        {
            writer.WriteStartElement("Con");
            writer.WriteStartElement("Name");
            writer.WriteString(clsCrypto.EncryptString(buName));
            writer.WriteEndElement();
            writer.WriteStartElement("Server");
            writer.WriteString(clsCrypto.EncryptString(Server));
            writer.WriteEndElement();
            writer.WriteStartElement("DataBase");
            writer.WriteString(clsCrypto.EncryptString(thisDB));
            writer.WriteEndElement();
            writer.WriteStartElement("AuthType");
            writer.WriteString(clsCrypto.EncryptString(authType));
            writer.WriteEndElement();
            writer.WriteStartElement("UserName");
            writer.WriteString(clsCrypto.EncryptString(userName));
            writer.WriteEndElement();
            writer.WriteStartElement("UserPW");
            writer.WriteString(clsCrypto.EncryptString(userPW));
            writer.WriteEndElement();
            writer.WriteStartElement("RegionID");
            writer.WriteString(clsCrypto.EncryptString(buID));
            writer.WriteEndElement();
            writer.WriteStartElement("RegionToken");
            writer.WriteString(clsCrypto.EncryptString(buToken));
            writer.WriteEndElement();   //RegionToken
            writer.WriteStartElement("ServerRegion");
            writer.WriteString(clsCrypto.EncryptString(connNameOnServer));
            writer.WriteEndElement();   //ServerRegion
            
            writer.WriteEndElement();   //Con
        }

        public static svcReturn testConnection(String Server, String dataBase, String authType, String userName, String userPW)
        {

            bool integratedSec = false;
            if (!(authType.CompareTo("SQL Server Authentication") == 0))
            {
                integratedSec = true;
                userName = "";
                userPW = "";
            }
    
            //server=PC\\SQLEXPRESS;database= ;Integrated Security= false;user id=sa;password=!!gowings!!;
            //return ("server=" + connSettings[1]
            //            + ";database= " + dataBase
            //            + ";Integrated Security= " + connSettings[4]
            //            + ";user id=" + connSettings[5]
            //            + ";password=" + connSettings[6] + ";");

            svcReturn ret = clsShared.initValidReturn();
            String connectionString = ("server=" + Server);
            connectionString = connectionString + ";database=" + dataBase;
            connectionString = connectionString + ";Integrated Security=" + integratedSec.ToString();

            connectionString = connectionString + "; Uid=" + userName + "; Pwd=" + userPW;
            //server=PC\\SQLEXPRESS;database= ;Integrated Security= false;user id=sa;password=!!gowings!!;

            Int32 newProdID = 0;
            
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[" + "Vendprod" + "]')";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                string msg = "Could not connect to database.";
                try
                {

                    conn.Open();
                    msg = "Connected to server but vendor tables do not seem to exist in " + dataBase + ".";
                    newProdID = (Int32)cmd.ExecuteScalar();
                    if (newProdID == 0)
                    {
                        ret.errors.Add(msg);
                        ret.success = false;
                    }
                    else
                    {
                        ret.errors.Add("Connection Successful!");
                        ret.success = true;
                    }
                }
                catch (Exception ex)
                {
                    ret.success = false;
                    ret.errors.Add(msg + ".  The reported error was: " + ex.Message);
                }

            }
            //get the result value: 1-exist; 0-not exist;
            //MessageBox.Show(newProdID.ToString());


//            SqlConnection conn = new SqlConnection(connectionString);
//            try
//            {
//                conn.Open();
//                ret.errors.Add("Connection Successful!");
//            }
//            catch (Exception ex)
//            {
//                ret.success = false;
//                ret.errors.Add(ex.Message);
//            }
            return ret;
   
        }

        public static svcConnResponse removeConnection(List<svcConn> connList, String regionName)
        {
            svcConnResponse resp = new svcConnResponse();
            resp.success = false;

            foreach (svcConn oneConn in connList)
            {
                if (oneConn.name.CompareTo(regionName) == 0)
                {
                    resp.success = true;
                    connList.Remove(oneConn);
                    break;
                }
            }
            resp.connList = connList;          
            return resp;
        }

        public static svcConnResponse findConnection(List<svcConn> connList, String regionName)
        {
            svcConnResponse resp = new svcConnResponse();
            resp.success = false;

            foreach (svcConn oneConn in connList)
            {
                if (oneConn.name.CompareTo(regionName) == 0)
                {
                    resp.success = true;
                    resp.oneConnection = oneConn;
                    break;
                }
            }
            resp.connList = connList;
            return resp;
        }

    }
}


public class svcConn
{ 
    public String name {get; set;}
    public String HBRegion {get; set;}
    public String server {get; set;}
    public String database {get; set;}
    public String authType {get; set;}
    public String userName {get; set;}    //Auth0
    public String userPW { get; set; }  //Auth1
    public String buID { get; set; }
    public String compToken { get; set; }
    public String buToken { get; set; }

}



public class svcConnResponse
{
    public bool success { get; set; }
    public string message { get; set; }
    public svcConn oneConnection { get; set; }
    public List<svcConn> connList { get; set; }
}