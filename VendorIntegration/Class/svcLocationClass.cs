﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace VendorIntegration.Class
{
    class svcLocationClass
    {
        public static svcReturn sendLocationToServer(HttpClient RFIDWebService, registrationObject regObj, locationReq locReq)
        {
            svcReturn result = clsShared.initValidReturn();
            String url = "api/Cred/addUpdateLocation";
            try
            {
                HttpResponseMessage webResponse = RFIDWebService.PostAsJsonAsync(url, locReq).Result;

                if (webResponse.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    result = webResponse.Content.ReadAsAsync<svcReturn>().Result;
                    if (!result.success)
                    {
                        result.success = false;
                    }
                }
                else
                {
                    result.errors.Add("Could not call web service 100.  The reported error is: " + webResponse.ReasonPhrase);
                    result.success = false;
                }
            }
            catch (Exception ex)
            {
                result.errors.Add("Unhandled exception calling web service.  The reported error is: " + ex.GetBaseException());
                result.success = false;
            }
            return result;
        }

        public static svcReturn saveLocationsForRegion(HttpClient RFIDWebService, registrationObject regObj, svcConn oneConn, Label lblStatus)
        {
            svcReturn retObj = clsShared.initValidReturn();
            bool integratedSec = !(oneConn.authType.CompareTo("SQL Server Authentication") == 0);

            String connectionString = ("server=" + oneConn.server);
            connectionString = connectionString + ";database=" + oneConn.database;
            connectionString = connectionString + ";Integrated Security=" + integratedSec.ToString();

            connectionString = connectionString + "; Uid=" + oneConn.userName + "; Pwd=" + oneConn.userPW;

            //string sql = "SELECT * FROM ESTVEND";
            string sql = "SELECT * FROM ESTVEND ";
            if (oneConn.HBRegion.Trim().CompareTo("") != 0)
            {
                sql = "SELECT * FROM ESTVEND WHERE (REGION = '" + oneConn.HBRegion + "')";
            }
            
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                string msg = "Could not connect to database.";
                try
                {
                    SqlCommand comm = new SqlCommand(sql, conn);
                    conn.Open();
                    SqlDataReader theReader = comm.ExecuteReader();
                    while (theReader.Read())
                    {
                        locationReq locReq = new locationReq();
                        locReq.userGUID = regObj.userID;
                        locReq.token = regObj.token;
                        svcLocation loc = new svcLocation();
                        loc.contactList = new List<svcLocContact>();
                        loc.productList = new List<svcLocProduct>();

                        loc.Active = true;
                        loc.Address = theReader["ADDR1"].ToString().Trim(); ;
                        loc.Address2 = theReader["ADDR2"].ToString().Trim(); ;
                        loc.City = "";
                        loc.Code = theReader["Code"].ToString().Trim();
                        loc.ContactFirst = theReader["FIRST_NAME"].ToString().Trim();
                        loc.ContactLast = theReader["LAST_NAME"].ToString().Trim();
                        loc.Customer_Code = "";
                        loc.Description = theReader["Name"].ToString().Trim(); ;
                        loc.DivisionID = Guid.Parse(oneConn.buID);
                        loc.MainEmail = theReader["EMAIL"].ToString().Trim(); ;
                        loc.EndDate = DateTime.Now;
                        //loc.ID = "";
                        loc.MainPhone = theReader["PHONE"].ToString().Trim();
                        loc.RFIDTag = "";
                        loc.StartDate = DateTime.Now;
                        loc.State = theReader["State"].ToString().Trim(); ;
                        loc.Zip = theReader["Zip"].ToString().Trim(); ;
                        locReq.loc = loc;
                        lblStatus.Text = loc.Description;
                        lblStatus.Invalidate();

                        string sql2 = "Select * from VendCont where VENDOR_CODE = '" + loc.Code + "'";
                        using (SqlConnection conn2 = new SqlConnection(connectionString))
                        {
                            conn2.Open();
                            SqlCommand comm2 = new SqlCommand(sql2, conn2);
                            SqlDataReader contReader = comm2.ExecuteReader();
                            while (contReader.Read())
                            {
                                svcLocContact cont = new svcLocContact();
                                cont.active = true;
                                cont.email = contReader["email"].ToString().Trim();
                                cont.firstName = contReader["First_Name"].ToString().Trim();
                                cont.jobTitle = contReader["Job_Title"].ToString().Trim();
                                cont.lastName = contReader["Last_Name"].ToString().Trim();
                                cont.work = contReader["Phone_Num"].ToString().Trim();
                                cont.cell = contReader["Cell_Num"].ToString().Trim();
                                cont.fax = contReader["Fax_Num"].ToString().Trim();
                                loc.contactList.Add(cont);
                                if (loc.Code.Trim() == "PIP01")
                                {
                                    cont.active = true;
                                }
                                String isMain = contReader["MAIN_CONTACT"].ToString().ToUpper();
                                if (isMain.CompareTo("Y") == 0)
                                {
                                    loc.ContactFirst = cont.firstName;
                                    loc.ContactLast = cont.lastName;
                                }
                            }
                        }

                        string sql3 = "Select VENDPROD.PRODUCT as 'PROD', ESTPROD.[DESC] as 'DESCRIPTION' from VendProd LEFT OUTER JOIN ESTPROD on ESTPROD.PRODUCT = VendProd.PRODUCT where VENDOR_CODE = '" + loc.Code + "'";
                        using (SqlConnection conn3 = new SqlConnection(connectionString))
                        {
                            conn3.Open();
                            SqlCommand comm2 = new SqlCommand(sql3, conn3);
                            SqlDataReader prodReader = comm2.ExecuteReader();
                            while (prodReader.Read())
                            {
                                //Need to skip the 'This Vendor has no products"
                                String prodCode = prodReader["PROD"].ToString().Trim();
                                if (prodCode.CompareTo("*") != 0)
                                {
                                    svcLocProduct prod = new svcLocProduct();
                                    prod.active = true;
                                    prod.divisionID = "";
                                    prod.ID = Guid.Empty;
                                    prod.locationCode = loc.Code;
                                    prod.locationDesc = loc.Description;
                                    prod.locationID = "";
                                    prod.productCode = prodReader["PROD"].ToString().Trim();
                                    prod.productDesc = prodReader["DESCRIPTION"].ToString().Trim();
                                    prod.productID = "";
                                    prod.productCode = prodCode;
                                    loc.productList.Add(prod);
                                }
                            }
                        }

                        svcReturn tempRet = sendLocationToServer(RFIDWebService, regObj, locReq);
                        if (!tempRet.success)
                        { 
                            retObj.errors.Add("Could not add location " + loc.Description + ": " + tempRet.errors[0]);
                        }
                    }

                }
                catch (Exception ex)
                {
                    retObj.success = false;
                    retObj.errors.Add(msg + ".  The reported error was: " + ex.Message);
                }

            }
            return retObj;
        }

        public static svcReturn saveAllLocations(HttpClient RFIDWebService, List<svcConn> allConnections, registrationObject regObj, Label lblStatus )
        {
            svcReturn retObj = clsShared.initValidReturn();
            foreach (svcConn oneConn in allConnections)
            {
                svcReturn tempObj = saveLocationsForRegion(RFIDWebService, regObj, oneConn, lblStatus);
                if (!tempObj.success)
                {
                    retObj.success = false;
                    retObj.errors.AddRange(tempObj.errors);
                }
            }
            return retObj;
        }   
    }
}

public class locationReq
{
    public string userGUID;
    public string token;
    public svcLocation loc;
}

public class svcLocation
{
    public Guid ID { get; set; }
    public Guid DivisionID { get; set; }
    public string Customer_Code { get; set; }
    public string Code { get; set; }
    public string Description { get; set; }
    public Nullable<DateTime> StartDate { get; set; }
    public Nullable<DateTime> EndDate { get; set; }
    public DateTime Created { get; set; }
    public DateTime Updated { get; set; }
    public bool Active { get; set; }
    public string RFIDTag { get; set; }
    public string Address { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Zip { get; set; }
    public string ContactFirst { get; set; }
    public string ContactLast { get; set; }
    public string MainPhone { get; set; }
    public string MainEmail { get; set; }
    public List<svcLocContact> contactList { get; set; }
    public List<svcLocProduct> productList { get; set; }
}

public class svcLocContact
{
    public bool active { get; set; }
    public string id { get; set; }
    public string divisionID { get; set; }
    public string locationID { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string work { get; set; }
    public string cell { get; set; }
    public string fax { get; set; }
    public string email { get; set; }
    public string jobTitle { get; set; }
}

public class svcLocProduct
{
    public bool active { get; set; }
    public Guid ID { get; set; }
    public string divisionID { get; set; }
    public string locationID { get; set; }
    public string productID { get; set; }
    public string locationCode { get; set; }
    public string locationDesc { get; set; }
    public string productCode { get; set; }
    public string productDesc { get; set; }
}