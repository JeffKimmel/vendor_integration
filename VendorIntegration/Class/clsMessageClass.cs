﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Data.SqlClient;

namespace VendorIntegration.Class
{
    class clsMessageClass
    {
        public static svcReturn saveMessage(HttpClient RFIDWebService, messageObject reqObj)
        {
            svcReturn result = clsShared.initValidReturn();
            String url = "api/Cred/logOneMessage";
            try
            {
                HttpResponseMessage webResponse = RFIDWebService.PostAsJsonAsync(url, reqObj).Result;

                if (webResponse.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    result = webResponse.Content.ReadAsAsync<svcReturn>().Result;
                    if (!result.success)
                    {
                        result.success = false;
                    }
                }
                else
                {
                    result.errors.Add("Could not call web service 200.  The reported error is: " + webResponse.ReasonPhrase);
                    result.success = false;
                }
            }
            catch (Exception ex)
            {
                result.errors.Add("Unhandled exception 201 calling web service.  The reported error is: " + ex.GetBaseException());
                result.success = false;
            }
            return result;
        }

        public static svcReturn saveMessageList(HttpClient RFIDWebService, List <messageObject> reqObj)
        {
            svcReturn result = clsShared.initValidReturn();
            String url = "api/Cred/logMessages";
            try
            {
                HttpResponseMessage webResponse = RFIDWebService.PostAsJsonAsync(url, reqObj).Result;

                if (webResponse.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    result = webResponse.Content.ReadAsAsync<svcReturn>().Result;
                    if (!result.success)
                    {
                        result.success = false;
                    }
                }
                else
                {
                    result.errors.Add("Could not call web service 200.  The reported error is: " + webResponse.ReasonPhrase);
                    result.success = false;
                }
            }
            catch (Exception ex)
            {
                result.errors.Add("Unhandled exception 201 calling web service.  The reported error is: " + ex.GetBaseException());
                result.success = false;
            }
            return result;
        }

        public static messageObject createMsgObj(registrationObject regObj, String routine, String Message, bool successful)
        {
            messageObject req = new messageObject();
            req.App = "Vendor Integration Service";
            req.CompanyID = regObj.companyID;
            req.CreatedDateTime = DateTime.Now;
            req.DivisionID = regObj.divisionID;
            req.GroupingID = Guid.Empty;
            req.LogDateTime = DateTime.Now;
            req.Message = Message;
            req.Routine = routine;
            req.Successful = successful;
            try
            {
                req.WebAppUserID = Guid.Parse(regObj.userID);
            }
            catch (Exception)
            {
                req.WebAppUserID = Guid.Empty;
            }
            return req;
        }

        public static svcReturn initiateVendorUpload(HttpClient RFIDWebService, String companyID, string compToken, string userID, string userToken)
        {
            svcReturn regResp = clsShared.initValidReturn();

            String url = "api/Cred/initiateVendorUpload?companyID=" + companyID + "&token=" + compToken + "&userID=" + userID + "&userToken=" + userToken;
            try
            {
                HttpResponseMessage response = RFIDWebService.GetAsync(url).Result;
                if (!response.IsSuccessStatusCode)
                {
                    regResp.success = false;
                    regResp.errors.Add(response.ReasonPhrase);
                }
                else
                {
                    regResp = response.Content.ReadAsAsync<svcReturn>().Result;
                }
            }
            catch (Exception ex)
            {
                regResp.success = false;
                regResp.errors.Add("Unhandled exception, could not verify user: " + ex.GetBaseException());
            }
            return regResp;
        }

        public static svcReturn completeVendorUpload(HttpClient RFIDWebService, String companyID, string compToken, string userID, string userToken, bool wasSuccessful)
        {
            svcReturn regResp = clsShared.initValidReturn();

            String url = "api/Cred/completeVendorUpload?companyID=" + companyID + "&token=" + compToken + "&userID=" + userID + "&userToken=" + userToken + "&wasSuccessful=" + wasSuccessful.ToString();
            try
            {
                HttpResponseMessage response = RFIDWebService.GetAsync(url).Result;
                if (!response.IsSuccessStatusCode)
                {
                    regResp.success = false;
                    regResp.errors.Add(response.ReasonPhrase);
                }
                else
                {
                    regResp = response.Content.ReadAsAsync<svcReturn>().Result;
                }
            }
            catch (Exception ex)
            {
                regResp.success = false;
                regResp.errors.Add("Unhandled exception, could not verify user: " + ex.GetBaseException());
            }
            return regResp;
        }

    
    }
}

public class messageObject
{
    public Guid ID;
    public Guid CompanyID;
    public Guid DivisionID;
    public Guid WebAppUserID;
    public Guid GroupingID;
    public DateTime LogDateTime;
    public String App;
    public String Routine;
    public String Message;
    public Boolean Successful;
    public DateTime CreatedDateTime;
}
