﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using VendorIntegration.Class;

namespace VendorIntegration
{
    public partial class Register : Form
    {
        public HttpClient RFIDWebService;
        public bool m_RegisterCompany;
        public registrationObject regObj;

        public Register()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void regCompany()
        {
            string token = txtToken.Text;
            string compName = txtCompanyName.Text;
            RegisterSoftwareResponse ret = clsRegistration.regSoftware(RFIDWebService, compName, token);

            if (ret.success)
            {
                clsRegistration.saveCompanyRegistration(ret, token);
                MessageBox.Show("Registration successful!", clsConstants.ERRHDR);
                this.DialogResult = DialogResult.OK;

            }
            else
            {
                MessageBox.Show("Registration not successful.  Please try again.", clsConstants.ERRHDR);
            }
        }

        private void regUser()
        {
            string password = txtToken.Text;
            string userName = txtCompanyName.Text;
            RegisterSoftwareResponse ret = clsRegistration.loginUser(RFIDWebService, regObj.companyID.ToString(), userName, password);

            if (ret.success)
            {
                clsRegistration.saveUserRegistration(ret);
                MessageBox.Show("Registration successful!", clsConstants.ERRHDR);
                this.DialogResult = DialogResult.OK;

            }
            else
            {
                MessageBox.Show("Registration not successful.  Please try again.", clsConstants.ERRHDR);
            }
        }

        private void btnRegisterCompany_Click(object sender, EventArgs e)
        {
            if (m_RegisterCompany)
            {
                regCompany();
            }
            else
            {
                regUser();
            }
        }

        private void Register_Load(object sender, EventArgs e)
        {
            if (!m_RegisterCompany)
            {
                lblName.Text = "User Name:";
                lblPassword.Text = "User Password";
            }
        }


    }
}
