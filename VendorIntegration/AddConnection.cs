﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VendorIntegration
{
    public partial class AddConnection : Form
    {
        public List<svcBU> regionList;
        public svcBU selectedBU;
        
        public AddConnection()
        {
            InitializeComponent();
        }

        private void FillGrid()
        {
            grdRegions.AllowUserToAddRows = true;

            foreach (svcBU theBU in regionList)
            {
                DataGridViewRow row = (DataGridViewRow)grdRegions.Rows[0].Clone();
                row.Cells[0].Value = theBU.buName;
                row.Cells[1].Value = theBU.id;
                grdRegions.Rows.Add(row);
            }
            grdRegions.AllowUserToAddRows = true;

        }

        private void AddConnection_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void OK()
        {
            selectedBU = new svcBU();
            selectedBU.buName = grdRegions.CurrentRow.Cells[0].Value.ToString();
            selectedBU.id = Guid.Parse(grdRegions.CurrentRow.Cells[1].Value.ToString());
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            OK();
        }

        private void grdRegions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           // OK();
        }

        private void grdRegions_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OK();
        }
    }
}
